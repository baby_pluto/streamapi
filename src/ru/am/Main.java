package ru.am;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        try (InputStreamReader input = new InputStreamReader(System.in, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(input)) {
            HashMap<String, Integer> words = new HashMap<>();

            Arrays.stream(reader.readLine()
                            .toLowerCase()
                            .split("[^\\da-zA-Zа-яёА-ЯЁ]+"))
                    .forEach(w -> {
                        if (words.containsKey(w)) words.put(w, words.get(w) + 1);
                        else words.put(w, 1);
                    });

            words.entrySet().stream()
                    .sorted((w1, w2) -> {
                        return w1.getValue().equals(w2.getValue()) ?
                                w1.getKey().compareTo(w2.getKey()) : w2.getValue().compareTo(w1.getValue());
                    })
                    .limit(10)
                    .forEach(w -> System.out.println(w.getKey()));

        } catch (IOException e) {
            System.out.println("Exception.");
        }

    }
}